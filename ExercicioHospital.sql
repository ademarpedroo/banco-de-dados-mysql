create database hospital;
use hospital;

create table PACIENTE (
	codigo int not null,
    nome varchar (80) not null,
    cpf int,
    sexo char (1),
    codClinica int,
    check (sexo in ('M', 'F') )
    );
create table CLINICA (
	codigo int not null,
    nome varchar (80) not null,
    local varchar (100), 
    primary key (codigo)
    );
    

ALTER TABLE PACIENTE ADD data date;

ALTER TABLE PACIENTE MODIFY cpf char(11);

ALTER TABLE PACIENTE  ADD PRIMARY KEY (codigo);

ALTER TABLE PACIENTE ADD CONSTRAINT Fk_codclinica foreign key (codClinica) references clinica (codigo);

ALTER TABLE PACIENTE CHANGE data DataNascimento varchar(8);

ALTER TABLE PACIENTE DROP COLUMN DataNascimento;

RENAME TABLE PACIENTE TO PacienteHospital;

ALTER TABLE PacienteHospital ADD planoSaude char(4) default 'GEAP' ;

ALTER TABLE PacienteHospital ADD CONSTRAINT Plano CHECK(planoSaude in('GEAP','UNIMED','SUL AMÉRICA'));

DROP TABLE PacienteHospital;

DROP DATABASE HOSPITAL;




    
