create database Produtos;
use Produtos;

create table ESTADO(
	codigo int primary key,
    nome varchar (40));
    
create table FABRICANTE (
	codigo varchar(3) primary key,
    nome varchar (30));

create table PRODUTO (
	codigo int primary key,
    nome varchar (30),
    preço numeric (6,2),
	codfabricante varchar(3),
    foreign key (codfabricante) references FABRICANTE (codigo));
    
create table CLIENTE (
	id int primary key,
    nome varchar (30),
    endereço varchar (30),
    cidade varchar (25),
    cep int,
    codEstado int,
    telefone varchar(20),
    desconto numeric(3,1),
    foreign key (codEstado) references ESTADO (codigo));

create table PEDIDO(
	id int primary key,
    tipo varchar (20),
    IdCliente int,
    dataEntrada date,
    valorTotal int,
    desconto int,
    dataEmbarque date,
	foreign key (IdCliente) references CLIENTE (id));
    
    insert into ESTADO (codigo,nome) values (1, 'Pernambuco');
    insert into ESTADO (codigo,nome) values (2, 'Pernambuco');
    insert into ESTADO (codigo,nome) values (3, 'Pernambuco');
    insert into ESTADO (codigo,nome) values (4, 'Pernambuco');
    insert into ESTADO (codigo,nome) values (5, 'Pernambuco');
    
    insert into FABRICANTE (codigo,nome) values ('A01', 'HP');
	insert into FABRICANTE (codigo,nome) values ('A02', 'Compaq');
	insert into FABRICANTE (codigo,nome) values ('A03', 'DELL');
	insert into FABRICANTE (codigo,nome) values ('B01', 'Laboremus');
	insert into FABRICANTE (codigo,nome) values ('B02', 'Agrale');
	insert into FABRICANTE (codigo,nome) values ('C01', 'Tramontina');
	
    
    insert into PRODUTO values (1001, 'Forrageira',3000,'B01');
	insert into PRODUTO values (1004, 'Faqueiro',0000,'C01');
	insert into PRODUTO values (1002, 'Laptop',3500,'A03');
	insert into PRODUTO values (1020, 'Laptop',5000,'A02');
	insert into PRODUTO values (1010, 'Impressora',295,'A01');
    insert into PRODUTO values (1030, 'Motor 5HP',0,'B02');
    
	insert into CLIENTE values (101, 'ANA CLAUDIA','RUA ALFA','JOÃO PESSOA',55555-555,1,'(81)9999-9999', 5.00);
    
    alter table ESTADO add column verba numeric (8,2);