CREATE DATABASE empresa;

use empresa;

CREATE TABLE departamento(
	codigo int not null,
    nome varchar(80) not null,
    PRIMARY KEY (codigo)
);

CREATE TABLE funcionario(
	codigo int not null,
    nome varchar(50) not null,
    cpf char(12) not null,
    salario numeric(5,2),
    codDept int
);

ALTER TABLE funcionario  ADD PRIMARY KEY (codigo);
ALTER TABLE funcionario ADD categoria VARCHAR(10) default 'Júnior' NOT NULL;

ALTER TABLE funcionario DROP COLUMN salario;
ALTER TABLE funcionario ADD nivel VARCHAR(40);

ALTER TABLE funcionario ADD CONSTRAINT nivel CHECK(categoria in('Junior','Pleno','Senior'));

ALTER TABLE funcionario DROP CONSTRAINT nivel;

ALTER TABLE funcionario MODIFY categoria char(25);

ALTER TABLE funcionario CHANGE nome nomeCompleto varchar(55);

RENAME TABLE funcionario TO empregado;

select * from funcionario;


